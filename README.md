# This repository moved #
Please use the new location: https://github.com/mintforpeople/robobo-hri-vision

# ROBOBO Vision Library #

Contains the source code of the different modules that made up the ROBOBO Vision Library. 

This includes:

- Camera module.
- Color detection module.
- Face detection module.

NOTE: All the development happens in the develop or feature branches. The master branch contains the last stable release.